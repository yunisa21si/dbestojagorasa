@extends('default')

@section('content')

	<div class="d-flex justify-content-end mb-3"><a href="{{ route('bahanbakus.create') }}" class="btn btn-info">Create</a></div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>produk</th>
				<th>jumlahstok</th>
				<th>satuan</th>
				<th>namasupplier</th>
				<th>status</th>

				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($bahanbakus as $bahanbaku)

				<tr>
					<td>{{ $bahanbaku->id }}</td>
					<td>{{ $bahanbaku->produk }}</td>
					<td>{{ $bahanbaku->jumlahstok }}</td>
					<td>{{ $bahanbaku->satuan }}</td>
					<td>{{ $bahanbaku->namasupplier }}</td>
					<td>{{ $bahanbaku->status }}</td>

					<td>
						<div class="d-flex gap-2">
                            <a href="{{ route('bahanbakus.show', [$bahanbaku->id]) }}" class="btn btn-info">Show</a>
                            <a href="{{ route('bahanbakus.edit', [$bahanbaku->id]) }}" class="btn btn-primary">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['bahanbakus.destroy', $bahanbaku->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
					</td>
				</tr>

			@endforeach
		</tbody>
	</table>

@stop
