@extends('default')

@section('content')

	@if($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				{{ $error }} <br>
			@endforeach
		</div>
	@endif

	{!! Form::open(['route' => 'bahanbakus.store']) !!}

		<div class="mb-3">
			{{ Form::label('produk', 'Produk', ['class'=>'form-label']) }}
			{{ Form::text('produk', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('jumlahstok', 'Jumlahstok', ['class'=>'form-label']) }}
			{{ Form::text('jumlahstok', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('satuan', 'Satuan', ['class'=>'form-label']) }}
			{{ Form::text('satuan', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('namasupplier', 'Namasupplier', ['class'=>'form-label']) }}
			{{ Form::text('namasupplier', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('status', 'Status', ['class'=>'form-label']) }}
			{{ Form::text('status', null, array('class' => 'form-control')) }}
		</div>


		{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}


@stop